#! /bin/bash
# Version 2.0 2016 01 13

# default values
interface=eth0
domain=www-auth.unhcr.org
url=https://$domain/cgi-bin/texis/vtx/mappmodules/getXML.xml
tcpdumpDo=false

# help
usage() { echo "Usage: $0 [-i interface] [-u url] -t (tcpdump - require sudo)" 1>&2; exit 1; }

# check flag/parameters
while getopts :i:u:ht flag; do
  case "${flag}" in
    i)
      interface="${OPTARG}"
      ;;
    u)
      url="${OPTARG}"
      domain=`echo $url | sed -e 's|^[^/]*//||' -e 's|/.*$||'`
      ;;
    t)
      if (( $EUID != 0 )); then
        echo "tcpdump option requires sudo"
        exit
      fi
      tcpdumpDo=true
      ;;
    h)
      usage
      ;;
  esac
done

# http://stackoverflow.com/questions/23828413/get-mac-address-using-shell-script
# works for both mac and linux as long the interface is correct
macAddress=`ifconfig $interface| grep -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}'`
macAddressEncode=`echo $macAddress| tr ":" "-"`

timestamp=`date +"%s"`

# set file
fileDump="$domain-dump-$macAddressEncode-$timestamp.pcap"
fileInfo="$domain-info-$macAddressEncode-$timestamp.txt"
fileTime="$domain-time-$macAddressEncode-$timestamp.csv"

# info
echo "interface: $interface" | tee -a $fileInfo
echo "url: $url" | tee -a $fileInfo
echo "domain: $domain" | tee -a $fileInfo
echo "tcpdump: $tcpdumpDo" | tee -a $fileInfo

# run tcpdump only if sudo
if $tcpdumpDo ; then

  # kill tcpdump if is running
  ps cax | grep tcpdump > /dev/null
  if [ $? -eq 0 ]; then
    killall -TERM tcpdump
    sleep 1
    killall -KILL  tcpdump
  fi

  # http://stackoverflow.com/questions/5161193/bash-script-that-kills-a-child-process-after-a-given-timeout
  echo "tcpdump -lnni $interface -s 65535  -w $fileDump host $domain" | tee -a $fileInfo
  (tcpdump -lnni $interface -s 65535  -w $fileDump host $domain) & pid=$!
fi

# kill tcpdump and zip files on SIG* termination
function clean_up {

  # kill tcpdump
  if $tcpdumpDo; then
    kill $pid
  fi
  sleep 1

  # zip results
  fileZip="$domain-test-$macAddressEncode-$timestamp.zip"
  zip $fileZip *$timestamp*
  exit
}

trap clean_up SIGHUP SIGINT SIGTERM

echo `date` > $fileInfo
echo "URL: $url" >> $fileInfo
printf "Get ifconfig info... "
ifconfig >> $fileInfo
printf "(mac=$macAddress) Done!\nGet traceroute... "
traceroute $domain  >> $fileInfo
printf "Done!\n"

echo "i, initial_timestamp, http_code, time_total, date" > $fileTime
i=0

while true
do
  ((i++))
#  ./mapp-curl.sh $url $i $fileTime &
  (
    initTime=`date +"%s"`
    curlRet=`curl -k -sL -w "%{http_code}, %{time_total}" "$url" -o /dev/null`
    httpCode=`echo $curlRet | cut -c1-3`
    OUTPUT=`echo " $i, $initTime, $curlRet, "; date`
    echo $OUTPUT | tee -a $fileTime
  ) &
  sleep 1
done

